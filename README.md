**Welcome to the Imperial College London Undergrad-level Computational Biostats repository!**

Here you will find all Notes and associated Code and Data that are part of the the Year 1 and Year 2 modules, as well as the Lectures. 

Please use the [Downloads link](https://bitbucket.org/mhasoba/iccompbiostat/downloads) to download the full repository, or inspect the [Source](https://bitbucket.org/mhasoba/iccompbiostat/src) and download specific files    